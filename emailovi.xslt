<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ad="http://viser.edu.rs">
    <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="ad:adresar">
        <html>
            <body>
                <h2>Osobe i kompanije sa više od dve email adrese:</h2>
                <br />
                <b>Osobe:</b>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Sifra</th>
                        <th>Ime</th>
                        <th>Srednje Ime</th>
                        <th>Prezime</th>
                        <th>Datum rođenja</th>
                        <th>Adresa</th>
                        <th>Mesto</th>
                        <th>Država</th>
                        <th>Telefon</th>
                        <th>Email</th>
                    </tr>
                    <xsl:for-each select="ad:osoba">
                        <xsl:if test="count(ad:email) &gt; 2">
                            <tr>
                                <td>
                                    <xsl:value-of select="@sifra" />
                                </td>
                                <td>
                                    <xsl:value-of select="ad:punoIme/ad:ime" />
                                </td>
                                <td>
                                    <xsl:value-of select="ad:punoIme/ad:srednjeIme" />
                                </td>
                                <td>
                                    <xsl:value-of select="ad:punoIme/ad:prezime" />
                                </td>
                                <td>
                                    <xsl:value-of select="ad:datumRodjenja/ad:dan" />.
                                    <xsl:value-of select="ad:datumRodjenja/ad:mesec" />.
                                    <xsl:value-of select="ad:datumRodjenja/ad:godina" />.
                                </td>
                                <td>
                                    <xsl:value-of select="ad:adresa/ad:ulica" />&#160;
                                    <xsl:value-of select="ad:adresa/ad:broj" />
                                    (<xsl:value-of select="ad:adresa/@tip" />)
                                </td>
                                <td>
                                    <xsl:value-of select="ad:adresa/ad:grad" />
                                </td>
                                <td>
                                    <xsl:value-of select="ad:adresa/ad:drzava" />
                                </td>
                                <td>
                                    <xsl:value-of select="ad:telefon" />
                                    (<xsl:value-of select="ad:telefon/@tip" />)
                                </td>
                                <td>
                                    <xsl:for-each select="ad:email">
                                        <xsl:value-of select="current()" />
                                        (<xsl:value-of select="@tip" />)
                                        <br />
                                    </xsl:for-each>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
                
                <hr />

                <br />
                <br />
                <b>Kompanije:</b>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Sifra</th>
                        <th>Naziv</th>
                        <th>Adresa</th>
                        <th>Mesto</th>
                        <th>Država</th>
                        <th>Telefon</th>
                        <th>Email</th>
                    </tr>
                    <xsl:for-each select="ad:kompanija">
                        <xsl:if test="count(ad:email) &gt; 2">
                            <tr>
                                <td>
                                    <xsl:value-of select="@sifra" />
                                </td>
                                <td>
                                    <xsl:value-of select="ad:naziv" />
                                </td>
                                <td>
                                    <xsl:value-of select="ad:adresa/ad:ulica" />&#160;
                                    <xsl:value-of select="ad:adresa/ad:broj" />
                                    (<xsl:value-of select="ad:adresa/@tip" />)
                                </td>
                                <td>
                                    <xsl:value-of select="ad:adresa/ad:grad" />
                                </td>
                                <td>
                                    <xsl:value-of select="ad:adresa/ad:drzava" />
                                </td>
                                <td>
                                    <xsl:value-of select="ad:telefon" />
                                    (<xsl:value-of select="ad:telefon/@tip" />)
                                </td>
                                <td>
                                    <xsl:for-each select="ad:email">
                                        <xsl:value-of select="current()" />
                                        (<xsl:value-of select="@tip" />)
                                        <br />
                                    </xsl:for-each>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
                <hr />
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>