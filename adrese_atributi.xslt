<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ad="http://viser.edu.rs">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" />
    <xsl:template match="ad:adresar">
        <adresar xmlns="http://viser.edu.rs" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://viser.edu.rs adresar.xsd">
            <xsl:for-each select="ad:osoba">
                <osoba sifra="{@sifra}">
                    <xsl:copy-of select="ad:punoIme" />
                    <xsl:copy-of select="ad:datumRodjenja" />
                    <adresa sifra="{ad:adresa/@tip}" ulica="{ad:adresa/ad:ulica}" broj="{ad:adresa/ad:broj}" grad="{ad:adresa/ad:grad}">
                        <drzava>
                            <xsl:value-of select="ad:adresa/ad:drzava" />
                        </drzava>
                    </adresa>
                    <xsl:copy-of select="ad:telefon" />
                    <xsl:for-each select="ad:email">
                        <xsl:copy-of select="current()" />
                    </xsl:for-each>
                </osoba>
            </xsl:for-each>

            <xsl:for-each select="ad:kompanija">
                <kompanija sifra="{@sifra}">
                    <xsl:copy-of select="ad:naziv" />
                    <adresa sifra="{ad:adresa/@tip}" ulica="{ad:adresa/ad:ulica}" broj="{ad:adresa/ad:broj}" grad="{ad:adresa/ad:grad}">
                        <drzava>
                            <xsl:value-of select="ad:adresa/ad:drzava" />
                        </drzava>
                    </adresa>
                    <xsl:copy-of select="ad:telefon" />
                    <xsl:for-each select="ad:email">
                        <xsl:copy-of select="current()" />
                    </xsl:for-each>
                </kompanija>
            </xsl:for-each>
        </adresar>
    </xsl:template>
</xsl:stylesheet>